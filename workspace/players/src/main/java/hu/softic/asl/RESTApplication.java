package hu.softic.asl;
// import the rest service you created!

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import hu.softic.asl.pages.IndexPage;
import hu.softic.asl.rest.AdminRest;
import hu.softic.asl.rest.CalendarRest;
import hu.softic.asl.rest.UsersRest;
import hu.softic.asl.rest.WorkdaysRest;
import hu.softic.asl.utils.CORSFilter;
import hu.softic.asl.utils.JWTRoleNeededFilter;

@ApplicationPath("/rest")
public class RESTApplication extends Application {

	public RESTApplication() {
	}

	/**
	 * Gets the classes.
	 *
	 * @return the classes
	 */
	@Override
	public Set<Class<?>> getClasses() {
		final Set<Class<?>> classes = new HashSet<>();

		classes.add(JWTRoleNeededFilter.class);
		classes.add(CORSFilter.class);

		classes.add(UsersRest.class);
		classes.add(CalendarRest.class);
		classes.add(AdminRest.class);
		classes.add(WorkdaysRest.class);

		classes.add(IndexPage.class);

		return classes;
	}
}