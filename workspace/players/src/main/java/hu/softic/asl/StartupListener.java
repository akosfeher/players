package hu.softic.asl;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

@WebListener
public class StartupListener implements ServletContextListener {
//	@Inject
//	protected static SettingsRepo settingsRepo;
//
//	protected static SettingsRepo getSettingsRepo() {
//		if (settingsRepo == null) {
//			settingsRepo = new SettingsRepo();
//		}
//		return settingsRepo;
//	}

	protected static String aslApiUrl;

	protected static boolean cronServer;

	public static ScheduledThreadPoolExecutor schedulerC;

	private static Client client;
	private static Map<String, WebTarget> target = new HashMap<>();

	@PostConstruct
	protected void init() {
		System.out.println("-------------------------  postconstruct inited");
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {

		aslApiUrl = ApplicationConfig.getInstance().getProperty("ASL_API_URL");
		client = ClientBuilder.newClient();
		cronServer = "true".equals(ApplicationConfig.getInstance().getProperty("CRON_SERVER"));

		// cronServer turned off
		// cronServer = false;
//		try {
//			gameChangeTime = ApplicationConfig.getInstance().getProperty("GAME_CHANGE_TIME");
//			if (gameChangeTime == null || gameChangeTime.isEmpty()) {
//				gameChangeTime = "10:05:00";
//			}
//
//			gameChangeHour = Integer.parseInt(gameChangeTime.substring(0, 2));
//			gameChangeMinute = Integer.parseInt(gameChangeTime.substring(3, 5));
//			gameChangeSec = Integer.parseInt(gameChangeTime.substring(6, 8));
//
//			gamePointsTime = ApplicationConfig.getInstance().getProperty("GAME_POINTS_TIME");
//			if (gamePointsTime == null || gamePointsTime.isEmpty()) {
//				gamePointsTime = "10:02:00";
//			}
//
//			gamePointsHour = Integer.parseInt(gamePointsTime.substring(0, 2));
//			gamePointsMinute = Integer.parseInt(gamePointsTime.substring(3, 5));
//			gamePointsSec = Integer.parseInt(gamePointsTime.substring(6, 8));
//
//		} catch (Exception e) {
//			gamePointsHour = 10;
//			gamePointsMinute = 2;
//			gamePointsSec = 0;
//
//			gameChangeHour = 10;
//			gameChangeMinute = 5;
//			gameChangeSec = 1;
//
//		}
//
//		System.out.println("------------- cronServer: " + cronServer + " contextInitialized gamePointsTime: "
//				+ gamePointsTime + " gameChangeTime: " + gameChangeTime);

//		if (cronServer) {
//
//			// daily points schedule
//			ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Budapest"));
//			ZonedDateTime nextRun = now.withHour(gamePointsHour).withMinute(gamePointsMinute).withSecond(gamePointsSec);
//
//			if (now.compareTo(nextRun) > 0)
//				nextRun = nextRun.plusDays(1);
//
//			Duration duration = Duration.between(now, nextRun);
//			long initalDelay = duration.getSeconds();
//
//			Runnable dailyPoints = () -> {
//				LocalDateTime now1 = LocalDateTime.now();
//				String retu = targetPoints.request(MediaType.APPLICATION_JSON).get(String.class);
//				System.out.println("Running...dailyPoints - now : " + now1 + "\nretu\n" + retu);
//			};
//
////			ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
//			schedulerC = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(1); // new
//																							// ScheduledThreadPoolExecutor(5);
//			schedulerC.setRemoveOnCancelPolicy(true);
//
//			ScheduledFuture<?> scheduledDayPoints = schedulerC.scheduleAtFixedRate(dailyPoints, initalDelay,
//					TimeUnit.DAYS.toSeconds(1), TimeUnit.SECONDS);
//
//			this.compScheduledDaypoints.put("softic", scheduledDayPoints);
//
//// day turn service, expired games change
//			ZonedDateTime now2 = ZonedDateTime.now(ZoneId.of("Europe/Budapest"));
//			ZonedDateTime nextRunC = now2.withHour(gameChangeHour).withMinute(gameChangeMinute)
//					.withSecond(gameChangeSec);
//
//			if (now2.compareTo(nextRunC) > 0)
//				nextRunC = nextRunC.plusDays(1);
//
//			Duration durationC = Duration.between(now2, nextRunC);
//			long initalDelayC = durationC.getSeconds();
//
//			Runnable dailyTurn = () -> {
//				LocalDateTime now3 = LocalDateTime.now();
//				String retuC = targetDayturn.request(MediaType.APPLICATION_JSON).get(String.class);
//				System.out.println("Running...dayTurn - now : " + now3 + "\nretu\n" + retuC);
//			};
//
//			ScheduledFuture<?> scheduledDayTurn = schedulerC.scheduleAtFixedRate(dailyTurn, initalDelayC,
//					TimeUnit.DAYS.toSeconds(1), TimeUnit.SECONDS);
//
//			this.compScheduledDayturns.put("softic", scheduledDayTurn);
//			// scheduledFuture.cancel(true);
//
//			System.out.println("ActiveCount: " + schedulerC.getActiveCount() + "  completed: "
//					+ schedulerC.getCompletedTaskCount());
//			System.out.println("dayTurn delay isn sec: " + scheduledDayTurn.getDelay(TimeUnit.SECONDS));
//		}

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		if (schedulerC != null) {
			schedulerC.shutdownNow();
		}
		ServletContext sc = sce.getServletContext();
		sc.removeAttribute("path");
		sc.removeAttribute("mode");
		System.out.println("Value deleted from context.");
	}
}