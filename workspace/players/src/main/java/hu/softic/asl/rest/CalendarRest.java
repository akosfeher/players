package hu.softic.asl.rest;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import hu.softic.asl.model.CalendarEvent;
import hu.softic.asl.model.EventEntry;
import hu.softic.asl.model.OrigCalendarResponse;
import hu.softic.asl.service.CalendarRepo;
import hu.softic.asl.service.Identifiable;
import hu.softic.asl.utils.GsonLocalDateTimeAdapter;

@Path("/calendar")
public class CalendarRest {

	private static Logger logger = Logger.getLogger(CalendarRest.class.getName());

	static final String SCHEMA = "asl";

	// @Inject
	protected static CalendarRepo calendarRepo;

	protected static CalendarRepo getCalendarRepo() {
		if (calendarRepo == null) {
			calendarRepo = new CalendarRepo();
		}
		return calendarRepo;
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}")
	// @JWTRoleNeeded({"ADMIN", "CEGADMIN"})
	// @EventLog(value = "USER_ALL")
	public Response getAllCalendarEvents(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String token,
			String payload) {

		String ret = getCalendarRepo().getAll(cegId, false);

		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new GsonLocalDateTimeAdapter()).create();

		List<CalendarEvent> calEvs = gson.fromJson(ret, new TypeToken<List<CalendarEvent>>() {
		}.getType());

		List<EventEntry> events = new ArrayList<>();

		for (CalendarEvent ev : calEvs) {
			events.add(ev.getEvent());
		}

		OrigCalendarResponse ocr = new OrigCalendarResponse(events);

		String ret1 = gson.toJson(ocr);

		return Response.status(200).entity(ret1).build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/event")
	public Response getEvents(@QueryParam("start") String start, @QueryParam("end") String enf) throws IOException {

		InputStream inputStream = CalendarRest.class.getResourceAsStream("/events.json");
		if (inputStream == null) {
			inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("/events.json");
		}

		String testevents = org.apache.commons.io.IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());

		return Response.status(200).entity(testevents).build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/events/{clientId}/{startDate}/{endDate}")
	public Response getMonthEvents(@PathParam("clientId") String clientId, @PathParam("startDate") String startDate,
			@PathParam("endDate") String endDate) throws IOException {

		String date;

		String ret = getCalendarRepo().getBetweenDates(SCHEMA, "DAY", startDate, endDate); // getByFields(SCHEMA,
																							// fieldNames, fieldTypes,
																							// fieldValues, true);

		return Response.status(200).entity(ret).build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/monthevents/{clientId}/{month}")
	public Response getMonthEvents(@PathParam("clientId") String clientId, @PathParam("month") String month)
			throws IOException {

		String[] fieldNames = { "MONTH", "CLIENTID" };
		String[] fieldTypes = { "STRING", "LONG" };
		String[] fieldValues = { month, clientId };

		String ret = getCalendarRepo().getByFields(SCHEMA, fieldNames, fieldTypes, fieldValues, true);

		return Response.status(200).entity(ret).build();
	}

	// date yyyy-MM num = 1 or -1
	private static String modifyMonth(String date, int num) {
		int year = Integer.parseInt(date.substring(0, 4));
		int monthNum = Integer.parseInt(date.substring(5, 7));

		if (num == -1 && monthNum == 1) {
			year--;
			monthNum = 12;
		} else if (num == 1 && monthNum == 12) {
			year++;
			monthNum = 1;
		} else {
			monthNum += num;
		}
		String monthStr = (monthNum < 10 ? "0" : "") + monthNum;
		String modDate = year + "-" + monthStr;

		return modDate;
	}

	@POST
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/event/{date}/{clientId}")
	public Response addEvent(@PathParam("date") String date, @PathParam("clientId") long clientId, String payload)
			throws IOException {

		logger.info("------------ " + date + "   payload: " + payload);

		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(payload).getAsJsonObject();

		Identifiable root = new Identifiable(obj);

		String ret = getCalendarRepo().save(SCHEMA, root);

		return Response.status(200).entity("OK").build();
	}

	@GET
// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/gener")
	public Response gener(@QueryParam("start") String start, @QueryParam("end") String enf) throws IOException {

		Gson gson = new Gson();

		EventEntry.Data data = new EventEntry.Data();

		data.setTitle("Title");
		data.setInvite("inviteds");

		EventEntry.Schedule schedule = new EventEntry.Schedule();
		schedule.setDuration((float) 12.2);
		schedule.addYear("2020");
		schedule.addMonth("06");
		schedule.addDayOfMonth("11");

		EventEntry eventr = new EventEntry();

		eventr.setData(data);
		eventr.setSchedule(schedule);

		String eventrStr = gson.toJson(eventr);

		System.out.println(eventrStr);

		InputStream inputStream = CalendarRest.class.getResourceAsStream("/evententry.json");
		if (inputStream == null) {
			inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("/evententry.json");
		}

		String testevents = org.apache.commons.io.IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());

		EventEntry ev = gson.fromJson(testevents, EventEntry.class);

		CalendarEvent calev = new CalendarEvent();
		calev.setEvent(ev);
		calev.setEvent_category("social");
		calev.setEvent_type("cardgame");
		LocalDateTime ldt = LocalDateTime.now().minusDays(2);
		calev.setTime(ldt.toString());

		String jsonStr = gson.toJson(calev);

		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(jsonStr).getAsJsonObject();
		Identifiable root = new Identifiable(obj);
		String id = getCalendarRepo().save(SCHEMA, root);

		return Response.status(200).entity(jsonStr).build();
	}

//	@GET
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("/{cegId}")
//	public Response getGames(@PathParam("cegId") String cegId, @QueryParam("active") boolean active) {
//
//		String ret = getUnitsRepo().getAll(cegId);
//
//		return Response.status(200).entity(ret).build();
//	}

//	@GET
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("/{cegId}/{id}")
//	public Response searchGamesById(@PathParam("cegId") String cegId, @PathParam("id") String id) {
//
//		Identifiable ifa = new Identifiable(Long.valueOf(id));
//
//		String ret = getUnitsRepo().getById(cegId, ifa, false);
//		return Response.status(200).entity(ret).build();
//	}

}