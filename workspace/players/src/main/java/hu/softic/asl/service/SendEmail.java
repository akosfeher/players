package hu.softic.asl.service;

import java.util.logging.Logger;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.HtmlEmail;

import hu.softic.asl.ApplicationConfig;

// Java program to send simple email using apache commons email
// Uses the Gmail SMTP servers
public class SendEmail {
	private static String smtpHost = ApplicationConfig.getInstance().getProperty("SMTP_HOST"); // = "smtp.gmail.com";
	private static int smtpPort; // = 465;
	private static boolean smtpSslFlag = "true".equals(ApplicationConfig.getInstance().getProperty("SMTP_SSL_FLAG")); // =
																														// true;
	private static String smtpUser = ApplicationConfig.getInstance().getProperty("SMTP_USER");
	private static String smtpPassword = ApplicationConfig.getInstance().getProperty("SMTP_PASSWORD");
	private static String emailFrom = ApplicationConfig.getInstance().getProperty("EMAIL_FROM");
	private static String testEmail = ApplicationConfig.getInstance().getProperty("TEST_EMAIL");
	private static boolean testEmailMode = "true"
			.equals(ApplicationConfig.getInstance().getProperty("TEST_EMAIL_MODE"));

	private static Logger logger = Logger.getLogger(SendEmail.class.getName());

	static {
		try {
			smtpPort = Integer.parseInt(ApplicationConfig.getInstance().getProperty("SMTP_PORT"));
		} catch (NumberFormatException e) {
			e.printStackTrace();
			smtpPort = 25;
		}
	}
//    private static String HOST = "smtp.softic.hu";
//    private static int PORT = 25;
//    private static boolean SSL_FLAG = false; 

	public static String sendSimpleEmail(boolean htmlemail, String toAddress, String subject, String htmlMessage,
			String textMessage, boolean testmode) {

//		String userName = "akosfeher54@gmail.com";
//		String password = "9999Qtya";
//        String userName = "feher@softic.hu";
//        String password = "rxo-N75q";

//		String fromAddress = "feher@softic.hu";

		if (toAddress.indexOf("testzzzz") > -1 && testEmail != null && testEmail.length() > 4) {
//			// String testEmail = System.getenv("CLIENT_URL");
//			String testEmail = ApplicationConfig.getInstance().getProperty("TEST_EMAIL");
//
//			if (testEmail == null || testEmail.isEmpty()) {
//				testEmail = "feher@itbt.hu";
//			}
//			// String testEmail = System.getProperty("TEST_EMAIL", "feher@itbt.hu");

			toAddress = testEmail;
		}
		String emailtest = "email to: " + toAddress + " subject: " + subject + " html: " + htmlMessage;
		if (testEmailMode || testmode) {
			logger.info(emailtest);
			return emailtest;
		} else {
			try {
				HtmlEmail email = new HtmlEmail();
				email.setHostName(smtpHost);
				email.setSmtpPort(smtpPort);
				email.setAuthenticator(new DefaultAuthenticator(smtpUser, smtpPassword));
				email.setSSLOnConnect(smtpSslFlag);
				email.setFrom(emailFrom);
				email.setSubject(subject);
				email.setHtmlMsg(htmlMessage);
				email.setTextMsg(textMessage);
				email.addTo(toAddress);
				email.send();
			} catch (Exception ex) {
				System.out.println("Unable to send email");
				ex.printStackTrace();
			}
		}
		return emailtest;
	}
}
