package hu.softic.asl.utils;

import java.io.IOException;
import java.util.logging.Logger;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

@Provider
public class CORSFilter implements ContainerResponseFilter {
	private static Logger logger = Logger.getLogger(CORSFilter.class.getName());

	@Override
	public void filter(final ContainerRequestContext requestContext, final ContainerResponseContext cres)
			throws IOException {
		String path = requestContext.getUriInfo().getPath();
		if (!path.contains("alive")) {
			logger.info("--------------------  " + path);
		}
//		String referer = requestContext.getHeaderString("referer");
//		String origin = "*";
//		if (path != null) {
//			int column = referer.indexOf(":", 8);
//			int end = referer.indexOf("/", column);
//			origin = referer.substring(0, end);
//
//			logger.info("--------- origin -----------  " + origin);
//		}
//		cres.getHeaders().add("Access-Control-Allow-Origin", origin);
		cres.getHeaders().add("Access-Control-Allow-Origin", "*");
		cres.getHeaders().add("Access-Control-Allow-Headers",
				"origin, content-type, accept, authorization, x-access-token");
		cres.getHeaders().add("Access-Control-Allow-Credentials", "true");
		cres.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
		// cres.getHeaders().remove("Allow");
		// cres.getHeaders().add("Allow", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
		cres.getHeaders().add("Access-Control-Max-Age", "1209600");
	}

}