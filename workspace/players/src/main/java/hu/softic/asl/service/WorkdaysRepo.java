package hu.softic.asl.service;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import hu.softic.asl.rest.WorkdaysRest.Workday;
import hu.softic.asl.utils.GsonLocalDateAdapter;

public class WorkdaysRepo extends AbstractRepo {
	@Override
	protected String tableName() {
		return "WORKDAYS_DIFF";
	}

	@Override
	protected String sequenceName() {
		return "workdays_diff_id_seq";
	}

	// dateStr yyyy-MM-dd
	public boolean isWorkday(String cegId, String dateStr) {
		String ret = getFirstByField(cegId, "DATE", "DATE", dateStr, false);

		boolean workday = true;
		if (ret == null || "[]".equals(ret)) {
			LocalDate localDate = LocalDate.parse(dateStr);
			workday = (localDate.getDayOfWeek().getValue() < 6);
		} else {
			Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new GsonLocalDateAdapter()).create();

			Workday workdayObj = gson.fromJson(ret, Workday.class);
			workday = workdayObj.isWorkday();
		}
		return workday;
	}

	public String fiveBefore(String cegId) {
		SimpleDateFormat sdfbefore = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -10);
		String dateStrbefore = sdfbefore.format(c.getTime());
		String dateStr = sdf.format(c.getTime());
		String jsonArray = getBetweenDateAndToday(cegId, "DATE", dateStrbefore);

		// database workday entries
		List<Workday> workdays = new ArrayList<>();
		if (jsonArray != null) {
			Type listType = new TypeToken<ArrayList<Workday>>() {
			}.getType();
			Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new GsonLocalDateAdapter()).create();
			workdays = gson.fromJson(jsonArray, listType);
		}

		Map<LocalDate, Boolean> dbWorkdaysMap = new HashMap<>();
		if (workdays.size() > 0) {
			for (Workday wd : workdays) {
				dbWorkdaysMap.put(wd.getLocalDate(), wd.isWorkday());
			}
		}
		LocalDate startDate = LocalDate.parse(dateStr);
		LocalDate endDate = LocalDate.now().plusDays(-1);
		int numOfWorkdays = 0;
		LocalDate d5 = LocalDate.now().plusDays(-5);
		for (LocalDate date = endDate; date.isAfter(startDate); date = date.plusDays(-1)) {
			boolean isWorkday = date.getDayOfWeek().getValue() < 6;
			if (dbWorkdaysMap.containsKey(date)) {
				isWorkday = dbWorkdaysMap.get(date);
			}
			if (isWorkday) {
				numOfWorkdays++;
			}
			if (numOfWorkdays == 5) {
				d5 = date;
				break;
			}
		}

		String d5Str = d5.toString();// sdf.format(asDate(d5));

		return d5Str;
	}

//	public static Date asDate(LocalDate localDate) {
//		return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
//	}

	@Override
	protected Set<String> leftoutColumns() {
		Set<String> cols = new HashSet<>();
		return cols;
	}

}
