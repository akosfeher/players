package hu.softic.asl.service;

public enum LogEventTypes implements LogEventTypesInterface {
	/*
	 * ADMIN_CREATE_CEG ADMIN_CREATE_CEG_ADMIN ADMIN_UPDATE_CEG
	 * ADMIN_DEACTIVATE_CEG_ADMIN
	 * 
	 * CEG_UPDATE CEG_CREATE_ADMIN CEG_DEACTIVATE_ADMIN CEG_CREATE_WEBSHOP_ADMIN
	 * CEG_DEACTIVATE_WEBSHOP_ADMIN CEG_ADD_UNIT CEG_REMOVE_UNIT CEG_UPDATE_UNIT
	 * 
	 * CEG_WEBSHOP_ADD_ITEM CEG_WEBSHOP_REMOVE_ITEM CEG_WEBSHOP_EDIT_ITEM
	 * CEG_WEBSHOP_ADD_ITEM_TO_SHOP CEG_WEBSHOP_REMOVE_ITEM_FROM_SHOP
	 * CEG_WEBSOP_ITEMS CEG_WEBSHOP_ACTIVE_ITEMS CEG_WEBSHOP_PURCHASE_REQUEST
	 * CEG_WEBSHOP_PURCHASE_CONFIRM CEG_WEBSHOP_PURCHASE_FULFILL
	 * CEG_WEBSHOP_PURCHASES
	 * 
	 */
	WORKDAY("Az adott nap munkanap e", "BOARD", "GET", "/rest/workdays/{cegId}/{date}", "", "", "USER"),
	WORKDAYS_SET("Az adott nap WORKDAY vagy HOLIDAY", "BOARD", "PUT", "/rest/workdays/{cegId}/{date}", "",
			"WORKDAY vagy HOLIDAY", "USER"),
	WORKDAYS_SINCE("Az adott dátumtó kezdve a mai nap hányadik munkanap", "BOARD", "GET",
			"/rest/workdays/{cegId}/since/{date}", "", "", "USER"),
	WORKDAYS_FIVEDAYSBEFORE("A mai dátum elötti ötödik munkanap", "BOARD", "GET",
			"/rest/workdays/{cegId}/fivedaysbefore", "", "", "USER"),

	BOARD_ALL("Az adott cég összes aktív táblája", "BOARD", "GET", "/rest/board/{cegId}", "", "", "CEGADMIN"),
	BOARD_GAME("Az adott tábla", "BOARD", "GET", "/rest/board/{cegId}/{gameId}",
			"form=fullList esetén minden boardEvent lejön", "", "CEGADMIN"),
	BOARD_FORMS("Az el�rhet� form�k", "BOARD", "GET", "/rest/board/forms", "", "", "USER"),
	BOARD_MOVE("Egy lépés kisérlet", "BOARD", "PUT", "/rest/board/{cegId}/{gameId}/{x}/{y}", "", "tb block", "USER"),
	BOARD_CANCEL("játék törlése", "BOARD", "GET", "/rest/board/{cegId}/{gameId}/canceled", "", "", "CEGADMIN"),
	BOARD_DAYTURN_POINTS("Napi forduló, új nap új pontok, 3 perccel a forduló elött", "BOARD", "GET",
			"/rest/board/{cegId}/dayturn", "", "", "CEGADMIN"),
	BOARD_DAYTURN("Napi forduló, expired játékok cseréje", "BOARD", "GET", "/rest/board/{cegId}/dayturn", "", "",
			"CEGADMIN"),
	BOARD_PLAYER_POINTS("Egy játékos pontjainak beállítása", "BOARD", "PUT",
			"/rest/board/{cegId}/{gameId}/points/{tp}", "", "points blocks day_player", "CEGADMIN"),
	BOARD_POINTS("Egy tábla pontjainak beállítása", "BOARD", "PUT", "/rest/board/{cegId}/{gameId}/points", "", "",
			"CEGADMIN"),

	USER_REGISTER("User registration", "USER", "POST", "/rest/users/{cegId}/register", "",
			"name, email, password, role", "USER"),
	USER_CONFIRM("User registration confirmation", "USER", "PUT", "/rest/users/{cegId}/confirmation/{token}", "", "",
			"USER"),
	USER_BYID("User query by id", "USER", "GET", "/rest/users/{cegId}/{userId}", "user data", "", "USER"),
	USER_LOGIN("User login", "USER", "POST", "/rest/users/{cegId}/login", "", "name, email, password, role,", "USER"),
	USER_ACT_GAME("User active game", "USER", "GET", "/rest/users/{cegId}/activegame/{userId}", "", "", "USER"),
	USER_ALL("User all", "USER", "GET", "/rest/users/{cegId}", "user data", "", "CEGADMIN");
	/*
	 * USER_PW_RESET_REQUEST("User pw reset request","USER", "PRODUCTION",
	 * "/rest/users/{cegId}/register","name, email, password, role,"),
	 * USER_PW_RESET("User pw reset","USER", "PRODUCTION",
	 * "/rest/users/{cegId}/register","name, email, password, role,"),
	 * 
	 * USER_UPDATE("User date update", "USER", "PUT",
	 * "/rest/users/{cegId}/{userId}", "user data"),
	 * USER_DEACTIVATE("User date update", "USER", "PUT",
	 * "/rest/users/{cegId}/{userId}", "user data"),
	 * 
	 * USER_CREATE_JWT("User date update", "USER", "PUT",
	 * "/rest/users/{cegId}/{userId}", "user data"),
	 * USERS_RESOLVE_JWT("User date update", "USER", "PUT",
	 * "/rest/users/{cegId}/{userId}", "user data"), USERS_PLAYING USERS_FREE
	 * USERS_PREPARED
	 * 
	 * TEAM_ALL("Companie all teams", "USER", "GET", "/rest/users/{cegId}/{userId}",
	 * "", "user data"), TEAM_BYID("User date update", "USER", "PUT",
	 * "/rest/users/{cegId}/{userId}", "user data"); TEAMS_ACTIVE TEAMS_PREPARED
	 * TEAM_CREATE
	 */

	/*
	 * GAME_DEF_CREATE GAME_DEF_UPDATE GAME_DEF_DELETE GAME_CREATE_FROM_PLAYERS
	 * GAME_CREATE_FROM_TEAMS GAMES_CREATE_FROM_DEF GAMES_CREATE_RAND GAMES_PREPARED
	 * GAMES_ACTIVE
	 * 
	 */

	private String desc;
	// ADMIN, CEG, USER, TEAM, GAME, BOARD
	private String subsystem;
	// GET, PUT, POST, DELETE
	private String mode;
	private String queryParams;
	private String payload;
	private String role;

	@Override
	public String getDesc() {
		return desc;
	}

	@Override
	public String getMode() {
		return mode;
	}

	@Override
	public String getSubsystem() {
		return subsystem;
	}

	LogEventTypes(String descr, String susystem, String mode, String url, String queryParams, String payloadDescr,
			String role) {
		this.desc = descr;
		this.subsystem = susystem;
		this.mode = mode;
		this.queryParams = queryParams;
		this.payload = payloadDescr;
		this.role = role;
	}
}
