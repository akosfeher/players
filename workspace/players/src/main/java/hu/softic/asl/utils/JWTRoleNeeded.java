package hu.softic.asl.utils;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.ws.rs.NameBinding;

@Retention(RUNTIME)
@Target({ TYPE, METHOD })
@NameBinding
public @interface JWTRoleNeeded {
	String[] value() default { "USER" };
}