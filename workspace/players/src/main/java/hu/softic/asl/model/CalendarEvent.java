package hu.softic.asl.model;

import java.time.LocalDateTime;

public class CalendarEvent {
	protected long id;
	protected LocalDateTime created_at;
	protected long created_by;
	protected LocalDateTime modified_at;
	protected long modified_by;
	protected String event_type;
	protected String event_category;
	protected String time;
	protected EventEntry event;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDateTime getCreated_at() {
		return created_at;
	}

	public void setCreated_at(LocalDateTime created_at) {
		this.created_at = created_at;
	}

	public long getCreated_by() {
		return created_by;
	}

	public void setCreated_by(long created_by) {
		this.created_by = created_by;
	}

	public LocalDateTime getModified_at() {
		return modified_at;
	}

	public void setModified_at(LocalDateTime modified_at) {
		this.modified_at = modified_at;
	}

	public long getModified_by() {
		return modified_by;
	}

	public void setModified_by(long modified_by) {
		this.modified_by = modified_by;
	}

	public String getEvent_type() {
		return event_type;
	}

	public void setEvent_type(String event_type) {
		this.event_type = event_type;
	}

	public String getEvent_category() {
		return event_category;
	}

	public void setEvent_category(String event_category) {
		this.event_category = event_category;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public EventEntry getEvent() {
		return event;
	}

	public void setEvent(EventEntry event) {
		this.event = event;
	}

	@Override
	public String toString() {
		return "CalendarEvent [id=" + id + ", created_at=" + created_at + ", created_by=" + created_by
				+ ", modified_at=" + modified_at + ", modified_by=" + modified_by + ", event_type=" + event_type
				+ ", event_category=" + event_category + ", time=" + time + ", event=" + event + "]";
	}

}
