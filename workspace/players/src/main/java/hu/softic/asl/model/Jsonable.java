package hu.softic.asl.model;

import com.google.gson.JsonObject;

public interface Jsonable {
	public JsonObject getJsonObject();

	public String getJsonString();
}
