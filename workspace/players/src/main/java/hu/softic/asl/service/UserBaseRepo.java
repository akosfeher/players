package hu.softic.asl.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@ApplicationScoped
@Named
@ManagedBean
public class UserBaseRepo extends AbstractRepo {
	@Override
	protected String tableName() {
		return "users";
	}

	@PostConstruct
	public void init() {
		System.out.println("UserRepo created");
	}

	@Override
	protected String sequenceName() {
		return "users_id_seq";
	}

	@Override
	protected Set<String> leftoutColumns() {
		Set<String> cols = new HashSet<>();
		cols.add("hash");
		cols.add("salt");
		cols.add("portrait");
		return cols;
	}

	public boolean activate(String cegId, Identifiable root) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.activate(connection, cegId, root);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public boolean activate(Connection aConnection, String cegId, Identifiable anAggregateRoot) {
		PreparedStatement statement = null;
		try {
			statement = aConnection.prepareStatement(
					"update " + (cegId + "." + this.tableName()) + " set active = TRUE" + " where id = ?");

			statement.setLong(1, anAggregateRoot.identity());

			statement.executeUpdate();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			this.close(statement);
		}

	}

	public boolean setNewPassword(String cegId, Long id, String hash, String salt) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.setNewPassword(connection, cegId, id, hash, salt);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public boolean setNewPassword(Connection aConnection, String cegId, Long id, String hash, String salt) {
		PreparedStatement statement = null;
		try {
			statement = aConnection.prepareStatement(
					"update " + (cegId + "." + this.tableName()) + " set hash = ?, salt = ? " + " where id = ?");

			statement.setString(1, hash);
			statement.setString(2, salt);

			statement.setLong(3, id);

			statement.executeUpdate();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			this.close(statement);
		}

	}
}
