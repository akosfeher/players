package hu.softic.asl.utils;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class GsonLocalDateTimeAdapter implements JsonSerializer<LocalDateTime>, JsonDeserializer<LocalDateTime> {

	// private final DateFormat dateFormat;
	// private final DateFormat dateFormatUTC;

	private final DateTimeFormatter formatter;

	public GsonLocalDateTimeAdapter() {
		// dateFormatUTC = new SimpleDateFormat("MMM d, yyyy h:m:ss aa",
		// Locale.ENGLISH);
		// // This is the format I need
		// dateFormat = new SimpleDateFormat("yyyy-MM-dd, HH:mm:ss"); // This is the
		// format I need
		formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	}

	@Override
	public synchronized JsonElement serialize(LocalDateTime date, Type type,
			JsonSerializationContext jsonSerializationContext) {
		return new JsonPrimitive(date.format(formatter)/* dateFormat.format(date) */);
	}

	@Override
	public synchronized LocalDateTime deserialize(JsonElement jsonElement, Type type,
			JsonDeserializationContext jsonDeserializationContext) {

		try {
			String dd = jsonElement.getAsString();
			LocalDateTime ret;
			try {
				ret = LocalDateTime.parse(dd, formatter);
				// ret = dateFormat.parse(dd);
			} catch (Exception e) {
				System.out.println("LocalDateTime adapter deserializer exception, dd: " + dd);
				ret = null; // dateFormatUTC.parse(dd);
			}
			return ret;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
