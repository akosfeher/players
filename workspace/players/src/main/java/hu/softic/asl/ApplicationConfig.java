package hu.softic.asl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class ApplicationConfig {

	private static Properties properties;
	private static ApplicationConfig instance;
	private static String PROPERTYFILENAME = "players.properties";
	private static String DOMAINDIRECTORY = System.getProperty("jboss.domain.config.dir");
	private static String STANDALONEDIRECTORY = System.getProperty("jboss.server.config.dir");

	private static Logger logger = Logger.getLogger(ApplicationConfig.class.getName());

	private ApplicationConfig() {
		initProperties();
	}

	public static ApplicationConfig getInstance() {
		if (instance == null) {
			instance = new ApplicationConfig();
		}
		return instance;
	}

	private static void initProperties() {
		properties = new Properties();
		String fileName = (DOMAINDIRECTORY != null ? DOMAINDIRECTORY : STANDALONEDIRECTORY) + "/" + PROPERTYFILENAME;
		try (FileInputStream input = new FileInputStream(new File(fileName))) {
			properties.load(new InputStreamReader(input, Charset.forName("UTF-8")));
		} catch (Exception e) {
			logger.severe("Exception occured: " + e.getMessage());
		}
	}

	/**
	 * System environment variable has precedens over default.properties (Which is
	 * for default values)
	 * 
	 * @param propertyName
	 * @return
	 */
	public String getProperty(String propertyName) {
		String val = System.getenv(propertyName);
		if (val != null) {
			return val;
		}

		if (properties == null) {
			getInstance();
		}
		if (properties != null) {
			return properties.getProperty(propertyName);
		}
		return null;
	}

	public Map<String, String> getMapProperty(String propertyName, String masterKey) {
		ObjectMapper mapper = new ObjectMapper();
		String mapString = properties.getProperty(propertyName);
		Map<String, Map<String, String>> mainMap;
		try {
			mainMap = mapper.readValue(mapString, new TypeReference<Map<String, Map<String, String>>>() {
			});
			return mainMap.get(masterKey);
		} catch (IOException e) {
			logger.severe("IOException: " + e.getMessage());
		}
		return new HashMap<>();
	}

}
