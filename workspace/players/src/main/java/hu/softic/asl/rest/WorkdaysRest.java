package hu.softic.asl.rest;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import hu.softic.asl.service.Identifiable;
import hu.softic.asl.service.WorkdaysRepo;
import hu.softic.asl.utils.EventLog;
import hu.softic.asl.utils.GsonLocalDateAdapter;

@Path("/workdays")
public class WorkdaysRest {

	@Inject
	protected WorkdaysRepo workdaysRepo;

	protected WorkdaysRepo getWorkdaysRepo() {
		if (workdaysRepo == null) {
			workdaysRepo = new WorkdaysRepo();
		}
		return workdaysRepo;
	}

	// date format yyyy-mm-dd
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/{date}")
	@EventLog(value = "WORKDAY")
	public Response getWorkdayStatusAtDate(@PathParam("cegId") String cegId, @PathParam("date") String dateStr) {

		String ret = getWorkdaysRepo().getFirstByField(cegId, "DATE", "DATE", dateStr, false);

		String wd = "WORKDAY";
		if (ret == null || "[]".equals(ret)) {
			LocalDate localDate = LocalDate.parse(dateStr);
			wd = (localDate.getDayOfWeek().getValue() < 6) ? "WORKDAY" : "HOLIDAY";
		} else {
			Gson gson = new GsonBuilder().setDateFormat("MMM d, yyyy").create();

			Workday workdayObj = gson.fromJson(ret, Workday.class);
			wd = workdayObj.isWorkday() ? "WOKDAY" : "HOLIDAY";
		}
		return Response.status(200).entity(wd).build();
	}

	// date format yyyy-mm
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/month/{date}")
	@EventLog(value = "WORKDAY")
	public Response getMonthWorkdays(@PathParam("cegId") String cegId, @PathParam("date") String dateStr) {

		String[] st = dateStr.split("-");
		int year = Integer.parseInt(st[0]);
		int month = Integer.parseInt(st[1]);
		LocalDate first = LocalDate.of(year, month, 1);
		LocalDate last = first.withDayOfMonth(first.lengthOfMonth());

		String jsonArray = getWorkdaysRepo().getBetweenDates(cegId, "DATE", first.toString(), last.toString());

		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new GsonLocalDateAdapter()).create();

		// database workday entries
		List<Workday> workdays = new ArrayList<>();
		if (jsonArray != null) {
			Type listType = new TypeToken<ArrayList<Workday>>() {
			}.getType();

			workdays = gson.fromJson(jsonArray, listType);
		}

		Map<LocalDate, Boolean> dbWorkdaysMap = new HashMap<>();

		if (workdays.size() > 0) {
			for (Workday wd : workdays) {
				dbWorkdaysMap.put(wd.getLocalDate(), wd.isWorkday());
			}
		}

		List<String> workdaysOfTheMonth = new ArrayList<>();
		for (LocalDate date = first; date.isBefore(last.plusDays(1)); date = date.plusDays(1)) {
			boolean workByDefault = date.getDayOfWeek().getValue() < 6;
			Boolean workByDb = dbWorkdaysMap.get(date);
			if (workByDb == null) {
				if (workByDefault) {
					workdaysOfTheMonth.add(date.toString());
				}
			} else {
				if (workByDb.booleanValue()) {
					workdaysOfTheMonth.add(date.toString());
				}
			}
		}

		String wd = gson.toJson(workdaysOfTheMonth);

		return Response.status(200).entity(wd).build();
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/since/{date}")
	@EventLog(value = "WORKDAYS_SINCE")
	public Response getNumOfWorkdays(@PathParam("cegId") String cegId, @PathParam("date") String dateStr) {

		String jsonArray = getWorkdaysRepo().getBetweenDateAndToday(cegId, "DATE", dateStr);

		// database workday entries
		List<Workday> workdays = new ArrayList<>();
		if (jsonArray != null) {
			Type listType = new TypeToken<ArrayList<Workday>>() {
			}.getType();
			Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new GsonLocalDateAdapter()).create();
			workdays = gson.fromJson(jsonArray, listType);
		}

		Map<LocalDate, Boolean> dbWorkdaysMap = new HashMap<>();
		if (workdays.size() > 0) {
			for (Workday wd : workdays) {
				dbWorkdaysMap.put(wd.getLocalDate(), wd.isWorkday());
			}
		}
		LocalDate startDate = LocalDate.parse(dateStr);
		LocalDate endDate = LocalDate.now().plusDays(1);
		int numOfWorkdays = 0;
		for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
			boolean isWorkday = date.getDayOfWeek().getValue() < 6;
			if (dbWorkdaysMap.containsKey(date)) {
				isWorkday = dbWorkdaysMap.get(date);
			}
			if (isWorkday) {
				numOfWorkdays++;
			}
		}

		return Response.status(200).entity(numOfWorkdays).build();
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/fivedaysbefore")
	@EventLog(value = "WORKDAYS_FIVEDAYSBEFORE")
	public Response getFiveDaysBefore(@PathParam("cegId") String cegId) {

		String d5Str = getWorkdaysRepo().fiveBefore(cegId);

		return Response.status(200).entity(d5Str).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/{date}")
	@EventLog(value = "WORKDAYS_SET")
	public Response getNumOfWorkdays(@PathParam("cegId") String cegId, @PathParam("date") String dateStr, String type) {

		JsonObject obj = new JsonObject();
		obj.addProperty("date", dateStr);
		obj.addProperty("type", type);

		Identifiable root = new Identifiable(obj);

		getWorkdaysRepo().upsertByField(cegId, "DATE", "DATE", dateStr, root);

		return Response.status(200).entity("{\"date\": \"" + dateStr + "\",   \"type\": " + type + "}").build();
	}

	public static class Workday {
		protected Long id;
		protected LocalDate date;
		protected String type;

		public Long getId() {
			return id;
		}

		public LocalDate getLocalDate() {
//			LocalDate ld = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//			return ld;
			return date;
		}

		public LocalDate getDate() {
			return date;
		}

		public String getType() {
			return type;
		}

		public boolean isWorkday() {
			return "WORKDAY".equals(type);
		}

		public Workday(Long id, LocalDate date, String type) {
			super();
			this.id = id;
			this.date = date;
			this.type = type;
		}

	}
}