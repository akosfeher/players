package hu.softic.asl.model;

import java.io.Serializable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import hu.softic.asl.utils.Utils;

public class User implements Serializable, Jsonable {

	private static final long serialVersionUID = 6672396667209394622L;

	protected Long id;

	public Long getId() {
		return id;
	}

	protected String email;
	protected String name;
	protected String username;
	protected String role;
	protected boolean registered;
	protected String salt;
	protected String hash;
	protected String password;
	protected String gamegroup;
	protected String teamnum;
	protected String clients;

	public String getClients() {
		return clients;
	}

	public void setClients(String clients) {
		this.clients = clients;
	}

	public String getTeamnum() {
		return teamnum;
	}

	public void setTeamnum(String teamnum) {
		this.teamnum = teamnum;
	}

	public void setActive_game_id(long active_game_id) {
		this.active_game_id = active_game_id;
	}

	public String getGamegroup() {
		return gamegroup;
	}

	public void setGamegroup(String gamegroup) {
		this.gamegroup = gamegroup;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setCeg(String ceg) {
		this.ceg = ceg;
	}

	public boolean isRegistered() {
		return registered;
	}

	public void setRegistered(boolean registered) {
		this.registered = registered;
	}

	public String getCeg() {
		return ceg;
	}

	protected boolean active;
	protected long active_game_id;

	protected String ceg;

	public User() {
	}

	public User(String email, String password, String name, String role) {
		this.email = email;
		this.name = name;
		this.role = role;
		this.salt = Utils.getRandomSalt();
		this.hash = Utils.createHash(password, this.salt);
		this.active = false;
		this.password = password;
		this.registered = true;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", email=" + email + ", name=" + name + ", username=" + username + ", role=" + role
				+ ", registered=" + registered + ", gamegroup=" + gamegroup + ", teamnum=" + teamnum + ", clients="
				+ clients + ", active=" + active + ", active_game_id=" + active_game_id + ", ceg=" + ceg + "]";
	}

	public String getEmail() {
		return email;
	}

	public String getName() {
		return name;
	}

	public String getUsername() {
		return username;
	}

	public String getSalt() {
		return salt;
	}

	public String getHash() {
		return hash;
	}

	public String getRole() {
		return role;
	}

	public void setPassword(String password) {
		this.password = password;
		this.salt = Utils.getRandomSalt();
		this.hash = Utils.createHash(password, this.salt);
		this.registered = true;
	}

	public void setPassword() {
		this.salt = Utils.getRandomSalt();
		this.hash = Utils.createHash(this.password, this.salt);
		this.registered = true;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public boolean checkPw(String pw, String origHash, String salt) {
		return origHash.equals(Utils.createHash(pw, salt));
	}

	public boolean checkPw() {
		return this.hash.equals(Utils.createHash(this.password, this.salt));
	}

	public boolean checkPw(String aPassword) {
		try {
			return this.hash.equals(Utils.createHash(aPassword, this.salt));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	@Override
	public JsonObject getJsonObject() {
		JsonObject json = new JsonObject();
		json.addProperty("id", id);
		json.addProperty("hash", hash);
		json.addProperty("ceg", ceg);
		json.addProperty("role", role);
		json.addProperty("salt", salt);
		json.addProperty("email", email);
		json.addProperty("name", name);
		json.addProperty("username", username);
		json.addProperty("active", active);
		json.addProperty("active_game_id", active_game_id);
		return json;
	}

	@Override
	public String getJsonString() {
		Gson gson = new GsonBuilder().create();
		String prettyJson = gson.toJson(getJsonObject());
		return prettyJson;
	}

	public void setEmail(String email2) {
		this.email = email2;
	}

	public void setPasswordNoHash(String pw) {
		this.password = pw;
	}

	public long getActive_game_id() {
		return active_game_id;
	}

}
